import '@babel/polyfill'
import gulp from 'gulp'
import babel from 'gulp-babel'
import uglyfly from 'gulp-uglyfly'
import nodemon from 'gulp-nodemon'
import mocha from 'gulp-mocha'
import env from 'gulp-env'
import { spawn } from 'child_process'
import {
  migrate, mock
} from './src/models/index'

const OUT_DIR = './dist/'

gulp.task('build:dev', () => {
  return gulp.src([
    'node_modules/@babel/polyfill/dist/polyfill.js',
    'src/**/*.js'
  ])
    .pipe(babel())
    .pipe(gulp.dest(OUT_DIR))
})

gulp.task('build:prod', () => {
  return gulp.src([
    'node_modules/@babel/polyfill/dist/polyfill.js',
    'src/**/*.js'
  ])
    .pipe(babel())
    .pipe(uglyfly({
      mangle: true,
      compress: true
    }))
    .pipe(gulp.dest(OUT_DIR))
})

gulp.task('start:dev', gulp.series('build:dev', () => {
  return nodemon({
    script: OUT_DIR + 'index.js',
    tasks: [ 'build:dev' ],
    watch: [ 'src' ]
  })
}))

gulp.task('start', cb => {
  env({ vars: { NODE_ENV: 'production' } })
  let cmd = spawn('node', [ OUT_DIR + 'index.js' ], { stdio: 'inherit' })
  cmd.on('close', function (code) {
    cb(code)
  })
})

gulp.task('test', () => {
  env({ vars: {
    DB_LOGLEVEL: 'none',
    NODE_ENV: 'test'
  } })
  return gulp.src('test/*.js')
    .pipe(mocha({
      require: [ '@babel/register' ],
      timeout: 30000,
      exit: true
    }))
    .once('error', err => {
      console.error(err)
      process.exit(1)
    })
})

gulp.task('migrate', cb => {
  migrate().then(() => cb(0)).catch(cb)
})

gulp.task('migrate:undo', cb => {
  migrate({ direction: 'down' }).then(() => cb(0)).catch(cb)
})

gulp.task('load_fixtures', gulp.series('migrate:undo', 'migrate', cb => {
  mock().then(() => cb(0)).catch(cb)
}))

gulp.task('default', gulp.series('build:prod', 'start'))
