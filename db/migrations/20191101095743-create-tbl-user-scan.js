'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblUserScan', {
      PK_FK_User: {
        allowNull: false,
        references: {
          model: 'tblUsers',
          key: 'PK_UserID'
        },
        type: DataTypes.INTEGER
      },
      PK_FK_Booth: {
        allowNull: false,
        references: {
          model: 'tblBooths',
          key: 'PK_BoothID'
        },
        type: DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
      .then(() => {
        return queryInterface.sequelize.query('ALTER TABLE "tblUserScan" ADD CONSTRAINT "PK_UserScanPK" PRIMARY KEY ("PK_FK_User", "PK_FK_Booth")')
      })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tblUserScan')
  }
}
