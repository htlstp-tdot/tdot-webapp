'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblUsers', {
      PK_UserID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      Username: {
        allowNull: false,
        type: DataTypes.STRING(255)
      },
      Password: {
        allowNull: false,
        type: DataTypes.CHAR(60)
      },
      FK_Role: {
        allowNull: false,
        references: {
          model: 'tblRoles',
          key: 'PK_RoleID'
        },
        type: DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
  },
  down: queryInterface => queryInterface.dropTable('tblUsers')
}
