'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblBooths', {
      PK_BoothID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      FK_Subject: {
        allowNull: false,
        references: {
          model: 'tblSubjects',
          key: 'PK_SubjectID'
        },
        type: DataTypes.INTEGER
      },
      PointReward: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      Roomnumber: {
        allowNull: false,
        type: DataTypes.STRING(10)
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
  },
  down: queryInterface => queryInterface.dropTable('tblBooths')
}
