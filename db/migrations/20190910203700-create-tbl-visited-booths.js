'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblVisitedBooths', {
      FK_Visitor: {
        allowNull: false,
        references: {
          model: 'tblVisitors',
          key: 'PK_VisitorID'
        },
        type: DataTypes.INTEGER
      },
      FK_Booth: {
        allowNull: false,
        references: {
          model: 'tblBooths',
          key: 'PK_BoothID'
        },
        type: DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
      .then(() => {
        return queryInterface.sequelize.query('ALTER TABLE "tblVisitedBooths" ADD CONSTRAINT "PK_VisitedBoothID" PRIMARY KEY ("FK_Visitor", "FK_Booth")')
      })
  },
  down: queryInterface => queryInterface.dropTable('tblVisitedBooths')
}
