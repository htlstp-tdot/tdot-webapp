'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblVisitors', {
      PK_VisitorID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      UQ_Email: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING(255)
      },
      Preregistration_IsAllowed: {
        allowNull: false,
        type: DataTypes.BOOLEAN
      },
      Score: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      AlreadyWon: {
        allowNull: false,
        type: DataTypes.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
  },
  down: queryInterface => queryInterface.dropTable('tblVisitors')
}
