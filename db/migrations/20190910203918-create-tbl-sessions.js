'use strict'
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('tblSessions', {
      PK_SessionID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      FK_User: {
        allowNull: false,
        references: {
          model: 'tblUsers',
          key: 'PK_UserID'
        },
        type: DataTypes.INTEGER
      },
      Key: {
        allowNull: false,
        type: DataTypes.CHAR(21)
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    })
  },
  down: queryInterface => queryInterface.dropTable('tblSessions')
}
