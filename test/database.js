import '@babel/polyfill'
import chai from 'chai'
import db, { mock } from '../src/models'

const { expect } = chai

describe('Test database', () => {
  it('should connect to db', async () => {
    let err = null
    try {
      await db.sequelize.authenticate()
    } catch (e) {
      err = e
    }
    expect(err).to.be.null
  })

  it('should run create mock data without error', async () => {
    let err = null
    try {
      await mock()
    } catch (e) {
      err = e
    }
    expect(err).to.be.null
  })
})
