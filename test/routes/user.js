import {
  error, expect, success
} from './util'
import models from '../../src/models'

export const getCurrentUserTest = {
  method: 'get',
  path: '/api/v1/user',
  mock: true,
  tests: [
    {
      name: 'should return alexzach userprofile',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.has.all.keys([
          'id',
          'username',
          'role',
          'scanPermissions'
        ])
        expect(body.id).to.equal(1)
        expect(body.username).to.equal('alexzach')
        expect(body.role.id).to.equal(1)
        expect(body.role.name).to.equal('admin')
        expect(body.role.power).to.equal(1000)
        expect(body.scanPermissions.length).to.equal(0)
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const createUserTest = {
  method: 'post',
  path: '/api/v1/user',
  mock: true,
  tests: [
    {
      name: 'should create user',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: {
          username: 'test',
          password: 'test',
          roleId: 1
        }
      },
      test: success(204, async () => {
        let users = await models.Users.findAll({ raw: true })
        expect(users.length).to.equal(4)
      })
    },
    {
      name: 'should return false roleId',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: {
          username: 'test',
          password: 'test',
          roleId: 0
        }
      },
      test: error(400)
    },
    {
      name: 'should return missing body',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getUsersTest = {
  method: 'get',
  path: '/api/v1/users',
  mock: true,
  tests: [
    {
      name: 'should return all users',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('array')
        expect(body.length).to.equal(3)
        let [ element ] = body
        expect(element).to.be.an('object').that.has.all.keys([
          'id',
          'username',
          'role',
          'scanPermissions'
        ])
        expect(element.id).to.equal(3)
        expect(element.username).to.equal('scan')
        expect(element.role).to.be.not.null
        expect(element.scanPermissions).to.be.an('array')
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getUserTest1 = {
  method: 'get',
  path: '/api/v1/user/1',
  mock: true,
  tests: [
    {
      name: 'should return user details',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.has.all.keys([
          'id',
          'username',
          'role',
          'scanPermissions'
        ])
        expect(body.id).to.equal(1)
        expect(body.username).to.equal('alexzach')
        expect(body.role.id).to.equal(1)
        expect(body.role.name).to.equal('admin')
        expect(body.role.power).to.equal(1000)
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getUserTest2 = {
  method: 'get',
  path: '/api/v1/user/a',
  mock: false,
  tests: [ {
    name: 'should return user not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const updateUserTest1 = {
  method: 'put',
  path: '/api/v1/user/2',
  mock: true,
  tests: [
    {
      name: 'should update user',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { password: 'test' }
      },
      test: success(204)
    },
    {
      name: 'should return no data to update',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const updateUserTest2 = {
  method: 'put',
  path: '/api/v1/user/0',
  mock: false,
  tests: [ {
    name: 'should return user not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const deleteUserTest1 = {
  method: 'delete',
  path: '/api/v1/user/2',
  mock: true,
  tests: [
    {
      name: 'should delete user',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { name: 'test' }
      },
      test: success(204)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const deleteUserTest2 = {
  method: 'delete',
  path: '/api/v1/user/1',
  mock: true,
  tests: [ {
    name: 'should return cannot delete own users',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(400)
  } ]
}

export const deleteUserTest3 = {
  method: 'delete',
  path: '/api/v1/user/0',
  mock: false,
  tests: [ {
    name: 'should return user not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}
