import {
  error, expect, success
} from './util'
import models from '../../src/models'

export const createRoleTest = {
  method: 'post',
  path: '/api/v1/role',
  mock: true,
  tests: [
    {
      name: 'should create role',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: {
          rolename: 'test',
          power: 1
        }
      },
      test: success(204, async () => {
        let roles = await models.Roles.findAll({ raw: true })
        expect(roles.length).to.equal(4)
      })
    },
    {
      name: 'should return missing body',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getRolesTest = {
  method: 'get',
  path: '/api/v1/roles',
  mock: true,
  tests: [
    {
      name: 'should return all roles',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        let [ element ] = body
        expect(element).to.be.an('object').that.has.all.keys([
          'id',
          'name',
          'power'
        ])
        expect(element.id).to.equal(1)
        expect(element.name).to.equal('admin')
        expect(element.power).to.equal(1000)
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getRoleTest1 = {
  method: 'get',
  path: '/api/v1/role/1',
  mock: true,
  tests: [
    {
      name: 'should return role details',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.has.all.keys([
          'id',
          'name',
          'power',
          'users'
        ])
        expect(body.id).to.equal(1)
        expect(body.name).to.equal('admin')
        expect(body.power).to.equal(1000)
        expect(body.users).to.be.an('array')
        expect(body.users.length).to.equal(2)
        expect(body.users[0]).to.be.an('object').that.has.all.keys([
          'id',
          'username'
        ])
        expect(body.users[0].id).to.equal(1)
        expect(body.users[0].username).to.equal('alexzach')
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getRoleTest2 = {
  method: 'get',
  path: '/api/v1/role/a',
  mock: false,
  tests: [ {
    name: 'should return role not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const updateRoleTest1 = {
  method: 'put',
  path: '/api/v1/role/2',
  mock: true,
  tests: [
    {
      name: 'should update role',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { name: 'test' }
      },
      test: success(204)
    },
    {
      name: 'should return no data to update',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const updateRoleTest2 = {
  method: 'put',
  path: '/api/v1/role/1',
  mock: true,
  tests: [ {
    name: 'should return cannot change own role',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(400)
  } ]
}

export const updateRoleTest3 = {
  method: 'put',
  path: '/api/v1/role/0',
  mock: false,
  tests: [ {
    name: 'should return role not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const deleteRoleTest1 = {
  method: 'delete',
  path: '/api/v1/role/3',
  mock: true,
  tests: [
    {
      name: 'should delete role',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { name: 'test' }
      },
      test: success(204)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const deleteRoleTest2 = {
  method: 'delete',
  path: '/api/v1/role/1',
  mock: true,
  tests: [ {
    name: 'should return cannot delete role with users',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(400)
  } ]
}

export const deleteRoleTest3 = {
  method: 'delete',
  path: '/api/v1/role/0',
  mock: false,
  tests: [ {
    name: 'should return role not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}
