import { success } from './util'

const statusTest = {
  method: 'get',
  path: '/api/v1/status',
  tests: [ {
    name: 'should return status',
    test: success(200)
  } ]
}

export default statusTest
