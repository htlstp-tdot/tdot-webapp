import {
  error, expect, success
} from './util'

export const loginTest = {
  method: 'post',
  path: '/api/v1/login',
  mock: true,
  tests: [
    {
      name: 'should return existing session for alexzach',
      requestoptions: { body: {
        username: 'alexzach',
        password: 'password'
      } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.have.all.keys([ 'key' ])
        expect(body.key).to.equals('jNiAz6LWSJzgi62pU5NZu')
      })
    },
    {
      name: 'should return new session for admin',
      requestoptions: { body: {
        username: 'admin',
        password: 'password'
      } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.have.all.keys([ 'key' ])
      })
    },
    {
      name: 'should return bad request',
      test: error(400)
    },
    {
      name: 'should return user doesnt exist',
      requestoptions: { body: {
        username: 'teest',
        password: 'teest'
      } },
      test: error(400)
    },
    {
      name: 'should return false password',
      requestoptions: { body: {
        username: 'alexzach',
        password: 'teest'
      } },
      test: error(401)
    }
  ]
}

export const logoutTest = {
  method: 'post',
  path: '/api/v1/logout',
  mock: true,
  tests: [
    {
      name: 'should logout alexzach',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(204)
    },
    {
      name: 'should not logout alexzach',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(401)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}
