import chai from 'chai'

const { expect } = chai

export { expect }

const runIfFunc = async (func, ...params) => {
  if (func && func.constructor && func.constructor.name === 'Function') return func(...params)
  else if (func && func.constructor && func.constructor.name === 'AsyncFunction') return await func(...params)
}

export const errNull = test => async (err, response) => {
  expect(err).to.be.null
  await runIfFunc(test, response)
}

export const errNotNull = test => async err => {
  expect(err).to.be.not.null
  await runIfFunc(test, err)
}

export const success = (status, test) => errNull(async response => {
  expect(response).to.have.property('statusCode', status)
  await runIfFunc(test, response)
})

export const error = (status, test) => errNotNull(async err => {
  expect(err).to.have.property('statusCode', status)
  await runIfFunc(test, err)
})
