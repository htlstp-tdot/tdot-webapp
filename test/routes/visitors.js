import {
  error, expect, success
} from './util'
import models from '../../src/models'

export const createVisitorTest = {
  method: 'post',
  path: '/api/v1/visitor',
  mock: true,
  tests: [
    {
      name: 'should create visitor',
      requestoptions: { body: {
        email: 'mail@example.com',
        preregistration_isAllowed: true
      } },
      test: success(204, async () => {
        let visitors = await models.Visitors.findAll({ raw: true })
        expect(visitors.length).to.equal(3)
      })
    },
    {
      name: 'should return invalid email',
      requestoptions: { body: {
        email: 'mail',
        preregistration_isAllowed: true
      } },
      test: error(400)
    },
    {
      name: 'should return missing body',
      test: error(400)
      // expect(body).to.have.property('detail', 'Email and preregistration consent are required')
    }
  ]
}

export const getVisitorsTest = {
  method: 'get',
  path: '/api/v1/visitors',
  mock: true,
  tests: [
    {
      name: 'should return all visitors',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        let [ element ] = body
        expect(element).to.be.an('object').that.has.all.keys([
          'id',
          'email',
          'preregistration_isAllowed',
          'score',
          'alreadyWon'
        ])
        expect(element.email).to.equal('test@example.org')
        expect(element.preregistration_isAllowed).to.equal(true)
        expect(element.score).to.equal(0),
        expect(element.alreadyWon).to.equal(false)
      })
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getVisitorTest1 = {
  method: 'get',
  path: '/api/v1/visitor/' + encodeURIComponent('test1@example.org'),
  mock: true,
  tests: [ {
    name: 'should return visitor details',
    test: success(200, ({ body }) => {
      expect(body).to.be.an('object').that.has.all.keys([
        'id',
        'email',
        'preregistration_isAllowed',
        'score',
        'alreadyWon',
        'visitedBooths'
      ])
      expect(body.id).to.equal(2)
      expect(body.email).to.equal('test1@example.org')
      expect(body.preregistration_isAllowed).to.equal(false)
      expect(body.score).to.equal(100)
      expect(body.alreadyWon).to.equal(false)
      expect(body.visitedBooths).to.be.an('array')
      expect(body.visitedBooths.length).to.equal(2)
      expect(body.visitedBooths[0]).to.be.an('object').that.has.all.keys([
        'id',
        'pointReward',
        'roomnumber',
        'subject'
      ])
      expect(body.visitedBooths[0].id).to.equal(1)
      expect(body.visitedBooths[0].pointReward).to.equal(50)
      expect(body.visitedBooths[0].roomnumber).to.equal('N310')
      expect(body.visitedBooths[0].subject).to.be.an('object').that.has.all.key([
        'id',
        'name',
        'infotext'
      ])
      expect(body.visitedBooths[0].subject.id).equal(2)
      expect(body.visitedBooths[0].subject.name).equal('Betriebswirtschaft und Management (BWM)')
      expect(body.visitedBooths[0].subject.infotext).equal('Lorem impsum')
    })
  } ]
}

export const getVisitorTest2 = {
  method: 'get',
  path: '/api/v1/visitor/a',
  mock: false,
  tests: [ {
    name: 'should return visitor not found',
    test: error(404)
  } ]
}

export const createVisitTest1 = {
  method: 'post',
  path: '/api/v1/visitor/' + encodeURIComponent('test@example.org') + '/visit',
  mock: true,
  tests: [
    {
      name: 'should add visit to visitor',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { boothId: 1 }
      },
      test: success(204)
    },
    {
      name: 'should return booth not found',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { boothId: 0 }
      },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const createVisitTest2 = {
  method: 'post',
  path: '/api/v1/visitor/' + encodeURIComponent('test1@example.org') + '/visit',
  mock: true,
  tests: [ {
    name: 'should return booth already visited',
    requestoptions: {
      headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
      body: { boothId: 1 }
    },
    test: error(400)
  } ]
}

export const createVisitTest3 = {
  method: 'post',
  path: '/api/v1/visitor/a/visit',
  mock: true,
  tests: [ {
    name: 'should return visitor not found',
    requestoptions: {
      headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
      body: { boothId: 1 }
    },
    test: error(404)
  } ]
}


export const deleteVisitorTest1 = {
  method: 'delete',
  path: '/api/v1/visitor/test@example.org',
  mock: true,
  tests: [
    {
      name: 'should delete user',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(204)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const deleteVisitorTest2 = {
  method: 'delete',
  path: '/api/v1/visitor/0',
  mock: false,
  tests: [ {
    name: 'should return visitor not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const calculateWinnerTest = {
  method: 'get',
  path: '/api/v1/visitors/winner',
  mock: true,
  tests: [
    {
      name: 'should return a visitor without setting alreadyWon',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        qs: { nosave: true }
      },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.has.all.keys([
          'id',
          'email',
          'preregistration_isAllowed',
          'score',
          'alreadyWon'
        ])
        expect(body.alreadyWon).to.equal(false)
      })
    },
    {
      name: 'should return a visitor with setting alreadyWon',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(200, ({ body }) => {
        expect(body).to.be.an('object').that.has.all.keys([
          'id',
          'email',
          'preregistration_isAllowed',
          'score',
          'alreadyWon'
        ])
        expect(body.alreadyWon).to.equal(true)
      })
    },
    {
      name: 'should return no visitor',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}
