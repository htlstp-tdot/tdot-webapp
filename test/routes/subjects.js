
import {
  error, expect, success
} from './util'
import models from '../../src/models'

export const createSubjectTest = {
  method: 'post',
  path: '/api/v1/subject',
  mock: true,
  tests: [
    {
      name: 'should create subject',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: {
          name: 'test',
          infotext: 'test'
        }
      },
      test: success(204, async () => {
        let subjects = await models.Subjects.findAll({ raw: true })
        expect(subjects.length).to.equal(4)
      })
    },
    {
      name: 'should return missing body',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getSubjectsTest = {
  method: 'get',
  path: '/api/v1/subjects',
  mock: true,
  tests: [ {
    name: 'should return all subjects',
    test: success(200, ({ body }) => {
      let [ element ] = body
      expect(element).to.be.an('object').that.has.all.keys([
        'id',
        'name',
        'infotext'
      ])
      expect(element.id).to.equal(1)
      expect(element.name).to.equal('Programmieren (POS)')
      expect(element.infotext).to.equal('Lorem impsum')
    })
  } ]
}

export const getSubjectTest1 = {
  method: 'get',
  path: '/api/v1/subject/1',
  mock: true,
  tests: [ {
    name: 'should return subject details',
    test: success(200, ({ body }) => {
      expect(body).to.be.an('object').that.has.all.keys([
        'id',
        'name',
        'infotext'
      ])
      expect(body.id).to.equal(1)
      expect(body.name).to.equal('Programmieren (POS)')
      expect(body.infotext).to.equal('Lorem impsum')
    })
  } ]
}

export const getSubjectTest2 = {
  method: 'get',
  path: '/api/v1/subject/a',
  mock: false,
  tests: [ {
    name: 'should return subject not found',
    test: error(404)
  } ]
}

export const updateSubjectTest1 = {
  method: 'put',
  path: '/api/v1/subject/1',
  mock: true,
  tests: [
    {
      name: 'should update subject',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { name: 'dummy' }
      },
      test: success(204)
    },
    {
      name: 'should return no data to update',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const updateSubjectTest2 = {
  method: 'put',
  path: '/api/v1/subject/0',
  mock: false,
  tests: [ {
    name: 'should return subject not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const deleteSubjectTest1 = {
  method: 'delete',
  path: '/api/v1/subject/3',
  mock: true,
  tests: [
    {
      name: 'should delete subject',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(204)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const deleteSubjectTest2 = {
  method: 'delete',
  path: '/api/v1/subject/0',
  mock: false,
  tests: [ {
    name: 'should return subject not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const deleteSubjectTest3 = {
  method: 'delete',
  path: '/api/v1/subject/1',
  mock: false,
  tests: [ {
    name: 'should return cannot delete subject with booths',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(400)
  } ]
}
