import {
  error, expect, success
} from './util'
import models from '../../src/models'

export const createBoothTest = {
  method: 'post',
  path: '/api/v1/booth',
  mock: true,
  tests: [
    {
      name: 'should create booth',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: {
          subjectId: 1,
          pointReward: 50,
          roomnumber: '123'
        }
      },
      test: success(204, async () => {
        let booths = await models.Booths.findAll({ raw: true })
        expect(booths.length).to.equal(3)
      })
    },
    {
      name: 'should return missing body',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const getBoothsTest = {
  method: 'get',
  path: '/api/v1/booths',
  mock: true,
  tests: [ {
    name: 'should return all booths',
    test: success(200, ({ body }) => {
      let [ element ] = body
      expect(element).to.be.an('object').that.has.all.keys([
        'id',
        'pointReward',
        'roomnumber',
        'subject'
      ])
      expect(element.id).to.equal(1)
      expect(element.pointReward).to.equal(50)
      expect(element.roomnumber).to.equal('N310')
      expect(element.subject).to.be.an('object').that.has.all.keys([
        'id',
        'name'
      ])
      expect(element.subject.id).to.equal(2)
      expect(element.subject.name).to.equal('Betriebswirtschaft und Management (BWM)')
    })
  } ]
}

export const getBoothTest1 = {
  method: 'get',
  path: '/api/v1/booth/1',
  mock: true,
  tests: [ {
    name: 'should return booth details',
    test: success(200, ({ body }) => {
      expect(body).to.be.an('object').that.has.all.keys([
        'id',
        'pointReward',
        'roomnumber',
        'subject'
      ])
      expect(body.id).to.equal(1)
      expect(body.pointReward).to.equal(50)
      expect(body.roomnumber).to.equal('N310')
      expect(body.subject).to.be.an('object').that.has.all.keys([
        'id',
        'name',
        'infotext'
      ])
      expect(body.subject.id).to.equal(2)
      expect(body.subject.name).to.equal('Betriebswirtschaft und Management (BWM)')
      expect(body.subject.infotext).to.equal('Lorem impsum')
    })
  } ]
}

export const getBoothTest2 = {
  method: 'get',
  path: '/api/v1/booth/a',
  mock: false,
  tests: [ {
    name: 'should return booth not found',
    test: error(404)
  } ]
}

export const updateBoothTest1 = {
  method: 'put',
  path: '/api/v1/booth/1',
  mock: true,
  tests: [
    {
      name: 'should update role',
      requestoptions: {
        headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' },
        body: { pointReward: 20 }
      },
      test: success(204)
    },
    {
      name: 'should return no data to update',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: error(400)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const updateBoothTest2 = {
  method: 'put',
  path: '/api/v1/booth/0',
  mock: false,
  tests: [ {
    name: 'should return booth not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}

export const deleteBoothTest1 = {
  method: 'delete',
  path: '/api/v1/booth/2',
  mock: true,
  tests: [
    {
      name: 'should delete booth',
      requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
      test: success(204)
    },
    {
      name: 'should return unauthorized',
      test: error(401)
    }
  ]
}

export const deleteBoothTest2 = {
  method: 'delete',
  path: '/api/v1/booth/0',
  mock: false,
  tests: [ {
    name: 'should return role not found',
    requestoptions: { headers: { 'x-api-key': 'jNiAz6LWSJzgi62pU5NZu' } },
    test: error(404)
  } ]
}
