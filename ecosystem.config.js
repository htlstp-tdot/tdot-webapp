module.exports = { apps: [ {
  name: 'api',
  script: './dist/index.js',
  instances: 0,
  exec_mode: 'cluster',
  env_production: { 'NODE_ENV': 'production' }
} ] }
