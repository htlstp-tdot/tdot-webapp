# TdoT Webapp
node.js backend for tdot webapp

## Setup
1. `git@gitlab.com:hoelzlmanuel/tdot-webapp.git`
2. `npm install`
3. create local-database.json and docker-database.json

## Setup DB
1. `$ cd docker && docker-compose up -d && cd ..`
2. create databases specified in local-database.json
3. `$ npx sequelize-cli db:migrate`
4. `$ NODE_ENV="production" npx sequelize-cli db:migrate`
4. `$ NODE_ENV="test" npx sequelize-cli db:migrate`


## Run development server
```bash
$ npm run start:dev
```

## Test
```bash
$ npm test
```

## Run in docker container
```bash
$ docker build . -t <projectname>
$ docker run -p 8080:8080 <projectname>:latest
```