'use strict'
module.exports = (sequelize, DataTypes) => {
  const UserScan = sequelize.define('UserScans', {
    PK_FK_User: {
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tblUsers',
        key: 'PK_UserID'
      },
      type: DataTypes.INTEGER
    },
    PK_FK_Booth: {
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tblBooths',
        key: 'PK_BoothID'
      },
      type: DataTypes.INTEGER
    }
  }, { tableName: 'tblUserScan' })
  UserScan.associate = function(models) {
    UserScan.belongsTo(models.Users, { foreignKey: 'PK_FK_User',  })
    UserScan.belongsTo(models.Booths, { foreignKey: 'PK_FK_Booth' })
  }
  return UserScan
}
