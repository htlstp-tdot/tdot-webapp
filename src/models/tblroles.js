'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblRoles = sequelize.define('Roles', {
    PK_RoleID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    Rolename: { type: DataTypes.STRING(255) },
    Power: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, { tableName: 'tblRoles' })
  tblRoles.associate = function(models) {
    tblRoles.hasMany(models.Users, { foreignKey: 'FK_Role' })
  }
  return tblRoles
}
