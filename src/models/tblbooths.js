'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblBooths = sequelize.define('Booths', {
    PK_BoothID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    FK_Subject: {
      allowNull: false,
      references: {
        model: 'tblSubjects',
        key: 'PK_SubjectID'
      },
      type: DataTypes.INTEGER
    },
    PointReward: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    Roomnumber: {
      allowNull: false,
      type: DataTypes.STRING(10)
    }
  }, { tableName: 'tblBooths' })
  tblBooths.associate = function(models) {
    tblBooths.hasMany(models.VisitedBooths, { foreignKey: 'FK_Booth' })
    tblBooths.hasMany(models.UserScans, { foreignKey: 'PK_FK_Booth' })
    tblBooths.belongsTo(models.Subjects, { foreignKey: 'FK_Subject' })
  }
  return tblBooths
}
