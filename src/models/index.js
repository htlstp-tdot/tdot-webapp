'use strict'

import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'
import Umzug from 'umzug'
import sequelize_fixtures from 'sequelize-fixtures'
import getConfig from '../util/config_helper'

const env = process.env.NODE_ENV || 'development'
const configPath = __dirname + '/../../config/local-database.json'
let dbConfig = {}
if (fs.existsSync(configPath)) {
  dbConfig = require(configPath)[env]
}

if (process.env.TDOT_WEBAPP_DB_USERNAME) {
  dbConfig.username = process.env.TDOT_WEBAPP_DB_USERNAME
} else if (!dbConfig.hasOwnProperty('username')) {
  throw Error('Missing database username. Either set environment variable TDOT_WEBAPP_DB_USERNAME or create config/local-database.json')
}
if (process.env.TDOT_WEBAPP_DB_PASSWORD) {
  dbConfig.password = process.env.TDOT_WEBAPP_DB_PASSWORD
} else if (!dbConfig.hasOwnProperty('password')) {
  throw Error('Missing database password. Either set environment variable TDOT_WEBAPP_DB_PASSWORD or create config/local-database.json')
}
if (process.env.TDOT_WEBAPP_DB_DATABASE) {
  dbConfig.database = process.env.TDOT_WEBAPP_DB_DATABASE
} else if (!dbConfig.hasOwnProperty('database')) {
  throw Error('Missing database database. Either set environment variable TDOT_WEBAPP_DB_DATABASE or create config/local-database.json')
}
if (process.env.TDOT_WEBAPP_DB_HOST) {
  dbConfig.host = process.env.TDOT_WEBAPP_DB_HOST
} else if (!dbConfig.hasOwnProperty('host')) {
  throw Error('Missing database host. Either set environment variable TDOT_WEBAPP_DB_HOST or create config/local-database.json')
}
if (process.env.TDOT_WEBAPP_DB_DIALECT) {
  dbConfig.dialect = process.env.TDOT_WEBAPP_DB_DIALECT
} else if (!dbConfig.hasOwnProperty('dialect')) {
  throw Error('Missing database dialect. Either set environment variable TDOT_WEBAPP_DB_DIALECT or create config/local-database.json')
}
if (process.env.TDOT_WEBAPP_DB_PORT) {
  dbConfig.dialect = process.env.TDOT_WEBAPP_DB_PORT
} else if (!dbConfig.hasOwnProperty('port')) {
  throw Error('Missing database port. Either set environment variable TDOT_WEBAPP_DB_PORT or create config/local-database.json')
}


const basename = path.basename(__filename)
const config = {
  ...dbConfig,
  logging: getConfig('DB_LOGLEVEL') !== 'none' ? console.log : false
}
const db = {}

let sequelize
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config)
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config)
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

export default db

const umzug = new Umzug({
  storage: 'sequelize',

  storageOptions: { sequelize: sequelize },

  migrations: {
    params: [
      sequelize.getQueryInterface(),
      Sequelize
    ],
    path: path.join(__dirname, '..', '..', 'db', 'migrations')
  }
})

export const migrate = ({ direction } = { direction: 'up' }) => {
  if (direction === 'up') {
    return umzug.up()
  } else if (direction === 'down') {
    return umzug.down({ to: 0 })
  }
}

export const mock = async () => {
  await migrate({ direction: 'down' })
  await migrate()
  const files = [
    'roles',
    'users',
    'sessions',
    'subjects',
    'booths',
    'visitors',
    'visited-booth',
    'user-scan'
  ]
  for (let file of files) {
    await sequelize_fixtures.loadFile(path.resolve(__dirname, '..', '..', 'db', 'fixtures', `${file}.json`), db, { log: getConfig('DB_LOGLEVEL') !== 'none' ? console.log : () => {} })
  }
}
