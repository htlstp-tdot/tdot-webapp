'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblUsers = sequelize.define('Users', {
    PK_UserID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    Username: {
      allowNull: false,
      type: DataTypes.STRING(255)
    },
    Password: {
      allowNull: false,
      type: DataTypes.CHAR(60)
    },
    FK_Role: {
      allowNull: false,
      references: {
        model: 'tblRoles',
        key: 'PK_RoleID'
      },
      type: DataTypes.INTEGER
    }
  }, { tableName: 'tblUsers' })
  tblUsers.associate = function(models) {
    tblUsers.hasMany(models.Sessions, { foreignKey: 'FK_User' })
    tblUsers.hasMany(models.UserScans, { foreignKey: 'PK_FK_User' })
    tblUsers.belongsTo(models.Roles, { foreignKey: 'FK_Role' })
  }
  return tblUsers
}
