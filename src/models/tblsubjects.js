'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblSubjects = sequelize.define('Subjects', {
    PK_SubjectID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    Name: {
      allowNull: false,
      type: DataTypes.STRING(255)
    },
    Infotext: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, { tableName: 'tblSubjects' })
  tblSubjects.associate = function(models) {
    tblSubjects.hasMany(models.Booths, { foreignKey: 'FK_Subject' })
  }
  return tblSubjects
}
