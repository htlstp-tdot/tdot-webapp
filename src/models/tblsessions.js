'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblSessions = sequelize.define('Sessions', {
    PK_SessionID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    FK_User: {
      allowNull: false,
      references: {
        model: 'tblUsers',
        key: 'PK_UserID'
      },
      type: DataTypes.INTEGER
    },
    Key: {
      allowNull: false,
      type: DataTypes.CHAR(21)
    }
  }, { tableName: 'tblSessions' })
  tblSessions.associate = function(models) {
    tblSessions.belongsTo(models.Users, { foreignKey: 'FK_User' })
  }
  return tblSessions
}
