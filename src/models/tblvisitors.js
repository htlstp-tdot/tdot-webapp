'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblVisitors = sequelize.define('Visitors', {
    PK_VisitorID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    UQ_Email: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(255)
    },
    Preregistration_IsAllowed: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    Score: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    AlreadyWon: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, { tableName: 'tblVisitors' })
  tblVisitors.associate = function(models) {
    tblVisitors.hasMany(models.VisitedBooths, { foreignKey: 'FK_Visitor' })
  }
  return tblVisitors
}
