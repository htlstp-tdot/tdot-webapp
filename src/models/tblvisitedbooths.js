'use strict'
module.exports = (sequelize, DataTypes) => {
  const tblVisitedBooths = sequelize.define('VisitedBooths', {
    FK_Visitor: {
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tblVisitors',
        key: 'PK_VisitorID'
      },
      type: DataTypes.INTEGER
    },
    FK_Booth: {
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tblBooths',
        key: 'PK_BoothID'
      },
      type: DataTypes.INTEGER
    },
  }, { tableName: 'tblVisitedBooths' })
  tblVisitedBooths.associate = function(models) {
    tblVisitedBooths.belongsTo(models.Visitors, { foreignKey: 'FK_Visitor' })
    tblVisitedBooths.belongsTo(models.Booths, { foreignKey: 'FK_Booth' })
  }
  return tblVisitedBooths
}
