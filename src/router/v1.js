import express from 'express'
import runRoute, {
  createRole,
  createUser,
  createVisit,
  createVisitor,
  deleteRole,
  getCurrentUser,
  getRole,
  getRoles,
  getUser,
  getUsers,
  getVisitor,
  getVisitors,
  login,
  logout,
  mockRoute,
  removeUser,
  removeVisitor,
  status,
  updateRole,
  updateUser,
  createBooth,
  getBooths,
  getBooth,
  updateBooth,
  deleteBooth,
  createSubject,
  getSubjects,
  getSubject,
  updateSubject,
  deleteSubject,
  getRooms,
  calculateWinner,
  addScanPermission,
  removeScanPermission
} from '../routes'
import checkAuth from '../middleware/auth'
import swagger_ui_router from './v1_swagger_ui'

const { Router } = express

const router = Router()

if (process.env.SWAGGER_UI || !process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  router
    .use('/swagger-ui', swagger_ui_router)
}

if (process.env.MOCK_ROUTE || !process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  router
    .post('/mock', runRoute(mockRoute))
}

router
  .get('/status', runRoute(status))

  .post('/login', runRoute(login))
  .post('/logout', checkAuth(0), runRoute(logout))

  .get('/user', checkAuth(0), runRoute(getCurrentUser))
  .post('/user', checkAuth(1000), runRoute(createUser))
  .get('/users', checkAuth(1000), runRoute(getUsers))
  .get('/user/:id', checkAuth(1000), runRoute(getUser))
  .put('/user/:id', checkAuth(1000), runRoute(updateUser))
  .delete('/user/:id', checkAuth(1000), runRoute(removeUser))
  .post('/user/:id/scanPermissions/:boothId', checkAuth(1000), runRoute(addScanPermission))
  .delete('/user/:id/scanPermissions/:boothId', checkAuth(1000), runRoute(removeScanPermission))

  .post('/visitor', runRoute(createVisitor))
  .get('/visitors', checkAuth(1000), runRoute(getVisitors))
  .get('/visitor/:email', runRoute(getVisitor))
  .post('/visitor/:email/visit', checkAuth(100), runRoute(createVisit))
  .delete('/visitor/:email', checkAuth(1000), runRoute(removeVisitor))
  .get('/visitors/winner', checkAuth(1000), runRoute(calculateWinner))

  .post('/role', checkAuth(1000), runRoute(createRole))
  .get('/roles', checkAuth(1000), runRoute(getRoles))
  .get('/role/:id', checkAuth(1000), runRoute(getRole))
  .put('/role/:id', checkAuth(1000), runRoute(updateRole))
  .delete('/role/:id', checkAuth(1000), runRoute(deleteRole))

  .post('/booth', checkAuth(1000), runRoute(createBooth))
  .get('/booths', runRoute(getBooths))
  .get('/booth/:id', runRoute(getBooth))
  .put('/booth/:id', checkAuth(1000), runRoute(updateBooth))
  .delete('/booth/:id', checkAuth(1000), runRoute(deleteBooth))
  .get('/rooms', runRoute(getRooms))

  .post('/subject', checkAuth(1000), runRoute(createSubject))
  .get('/subjects', runRoute(getSubjects))
  .get('/subject/:id', runRoute(getSubject))
  .put('/subject/:id', checkAuth(1000), runRoute(updateSubject))
  .delete('/subject/:id', checkAuth(1000), runRoute(deleteSubject))

export default router
