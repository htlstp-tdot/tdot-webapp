import express from 'express'
import swaggerUi from 'swagger-ui-express'
import read_yml from 'read-yaml'
const swaggerDocument = read_yml.sync(__dirname + '/../../docs/v1.openapi.yml')

const { Router } = express

const router = Router()

router
  .use('/', swaggerUi.serve)
  .get('/', swaggerUi.setup(swaggerDocument))

export default router
