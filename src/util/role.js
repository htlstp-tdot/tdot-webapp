import models from '../models'
import {
  BadRequestError, NotFoundError, parse_offset_limit, parse_sort_direction
} from '.'

export const addRole = async ({
  rolename, power
}) => {
  await models.Roles.create({
    Rolename: rolename,
    Power: power
  })
}

export const getRoles = async query => {
  let offset_limit = parse_offset_limit(query)
  let sort = parse_sort_direction({
    query,
    namemap: {
      'id': 'PK_RoleID',
      'name': 'Rolename',
      'power': 'Power'
    }
  })
  let roles = await models.Roles.findAll({
    raw: true,
    ...offset_limit,
    order: sort
  })
  let cnt = await models.Roles.count()

  let offset = offset_limit.offset || 0
  let contentrange = `roles ${offset}-${offset + roles.length - 1}/${cnt}`

  return {
    '_data': roles.map(e => ({
      id: e.PK_RoleID,
      name: e.Rolename,
      power: e.Power
    })),
    '_headers': {
      'Content-Range': contentrange,
      'Access-Control-Expose-Headers': 'Content-Range'
    }
  }
}

const findRole = async id => {
  let role

  if (!isNaN(parseInt(id))) {
    role = await models.Roles.findOne({ where: { PK_RoleID: id } })
  }

  return role
}

export const getRole = async id => {
  let role = await findRole(id)

  if (!role) {
    throw new NotFoundError(`Role ${id} not found`)
  }

  let users = await models.Users.findAll({ where: { FK_Role: role.PK_RoleID } })

  return {
    id: role.PK_RoleID,
    name: role.Rolename,
    power: role.Power,
    users: users.map(e => ({
      id: e.PK_UserID,
      username: e.Username
    }))
  }
}

export const updateRole = async (id, data) => {
  let role = await findRole(id)

  if (!role) {
    throw new NotFoundError(`Role ${id} not found`)
  }

  let newData = {}
  if (data.hasOwnProperty('name')) {
    newData.Rolename = data.name
  }
  if (data.hasOwnProperty('power')) {
    newData.Power = data.power
  }

  if (Object.keys(newData).length === 0) {
    throw new BadRequestError('No data to update')
  }

  await models.Roles.update(newData, { where: { PK_RoleID: id } })
}

export const deleteRole = async id => {
  let role = await findRole(id)

  if (!role) {
    throw new NotFoundError(`Role ${id} not found`)
  }

  let userc = await models.Users.count({ where: { FK_Role: id } })

  if (userc > 0) {
    throw new BadRequestError('Cannot delete role with users')
  }

  await role.destroy()
}
