import models from '../models'
import {
  BadRequestError, NotFoundError,  parse_offset_limit, parse_sort_direction
} from '.'

const { Op } = models.Sequelize

export const getVisitors = async query => {
  let offset_limit = parse_offset_limit(query)
  let sort = parse_sort_direction({
    query,
    namemap: {
      'idNum': 'PK_VisitorID',
      'email': 'UQ_Email',
      'preregistration_isAllowed': 'Preregistration_IsAllowed',
      'score': 'Score',
      'alreadyWon': 'AlreadyWon'
    }
  })
  let visitors = await models.Visitors.findAll({
    ...offset_limit,
    order: sort
  })
  let cnt = await models.Visitors.count()

  let offset = offset_limit.offset || 0
  let contentrange = `visitors ${offset}-${offset + visitors.length - 1}/${cnt}`

  return {
    '_data': visitors.map(e => ({
      id: e.PK_VisitorID,
      email: e.UQ_Email,
      preregistration_isAllowed: e.Preregistration_IsAllowed,
      score: e.Score,
      alreadyWon: e.AlreadyWon
    })),
    '_headers': {
      'Content-Range': contentrange,
      'Access-Control-Expose-Headers': 'Content-Range'
    }
  }
}

const findVisitor = async email => {
  let visitor = await models.Visitors.findOne({ where: { UQ_Email: email }, })
  return visitor
}

export const createVisitor = async ({
  email, preregistration_isAllowed, score = 0, alreadyWon = false
}) => {
  let visitor = await findVisitor(email)

  if (visitor) {
    throw new BadRequestError('Visitor already exists')
  }

  if (!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
    throw new BadRequestError('Invalid email format')
  }

  await models.Visitors.create({
    UQ_Email: email,
    Preregistration_IsAllowed: preregistration_isAllowed,
    Score: score,
    AlreadyWon: alreadyWon
  })
}

export const getVisitor = async email => {
  let visitor = await findVisitor(email)

  if (!visitor) {
    throw new NotFoundError(`${email} not found`)
  }

  let visitedBooths = await models.VisitedBooths.findAll({
    where: { FK_Visitor: visitor.PK_VisitorID },
    include: [ {
      model: models.Booths,
      include: [ models.Subjects ]
    } ]
  })

  return {
    id: visitor.PK_VisitorID,
    email: visitor.UQ_Email,
    preregistration_isAllowed: visitor.Preregistration_IsAllowed,
    score: visitor.Score,
    alreadyWon: visitor.AlreadyWon,
    visitedBooths: visitedBooths.map(e => ({
      id: e.Booth.PK_BoothID,
      pointReward: e.Booth.PointReward,
      roomnumber: e.Booth.Roomnumber,
      subject: {
        id: e.Booth.Subject.PK_SubjectID,
        name: e.Booth.Subject.Name,
        infotext: e.Booth.Subject.Infotext
      }
    }))
  }

}

export const createVisit = async (email, boothId) => {
  let visitor = await findVisitor(email)

  if (!visitor) {
    throw new NotFoundError(`${email} not found`)
  }

  let visitedBooth = await models.VisitedBooths.findOne({ where: {
    FK_Visitor: visitor.PK_VisitorID,
    FK_Booth: boothId
  } })

  if (visitedBooth) {
    throw new BadRequestError(`Booth ${boothId} already visited`)
  }

  let booth = await models.Booths.findOne({ where: { PK_BoothID: boothId } })

  if (!booth) {
    throw new BadRequestError(`Booth ${boothId} doesn't exist`)
  }

  await models.sequelize.transaction(t => {
    return models.VisitedBooths.create({
      FK_Visitor: visitor.PK_VisitorID,
      FK_Booth: booth.PK_BoothID
    }, { transaction: t }).then(() => {
      return visitor.update({ Score: visitor.Score + booth.PointReward }, { transaction: t })
    })
  })
}

export const removeVisitor = async email => {
  let visitor = await findVisitor(email)

  if (!visitor) {
    throw new NotFoundError(`${email} not found`)
  }

  await models.sequelize.transaction(t => {
    return models.VisitedBooths.destroy({ where: { FK_Visitor: visitor.PK_VisitorID } }, { transaction: t }).then(() => {
      return visitor.destroy({ transaction: t })
    })
  })
}

export const calculateWinner = async ({ nosave }) => {
  let visitors = await models.Visitors.findAll({
    where: {
      AlreadyWon: false,
      Score: { [Op.gt]: 0 }
    },
    attributes: [
      'PK_VisitorID',
      'Score'
    ]
  })

  if (visitors.length === 0) {
    throw new BadRequestError('No visitors found')
  }

  visitors[0].ScoreCumulative = visitors[0].Score
  for (let i = 1; i < visitors.length; i++) {
    visitors[i].ScoreCumulative = visitors[i].Score + visitors[i-1].ScoreCumulative
  }

  let total = visitors[visitors.length - 1].ScoreCumulative
  let winnerTicket = Math.floor(Math.random() * total)
  let i = 0
  while (winnerTicket >= visitors[i++].ScoreCumulative);
  let winner = await models.Visitors.findOne({ where: { PK_VisitorID: visitors[--i].PK_VisitorID } })
  if (nosave !== 'true') {
    winner.update({ AlreadyWon: true })
  }

  return {
    id: winner.PK_VisitorID,
    email: winner.UQ_Email,
    preregistration_isAllowed: winner.Preregistration_IsAllowed,
    score: winner.Score,
    alreadyWon: winner.AlreadyWon,
  }
}
