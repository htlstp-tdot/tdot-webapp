import models from '../models'
import bcrypt from 'bcrypt'
import nanoid from 'nanoid'
import {
  BadRequestError, UnauthorizedError
} from './errors'

const getUser = async username => {
  return models.Users.findOne({
    where: { Username: username },
    include: [ models.Roles ],
    raw: true
  })
}

export const logIn = async ({
  username, password
}) => {
  const user = await getUser(username)
  if (!user) throw new BadRequestError('User doesn\'t exist')
  if (await bcrypt.compare(password, user.Password.trim())) {
    const key = nanoid()
    const [ session ] = (await models.Sessions.findOrCreate({
      where: { FK_User: user.PK_UserID },
      defaults: {
        FK_User: user.PK_UserID,
        Key: key
      }
    }))
    return { key: session.Key }
  } else {
    throw new UnauthorizedError('Invalid password')
  }
}

export const logOut = async ({ key }) => {
  await models.Sessions.destroy({ where: { Key: key } })
}
