export const parse_offset_limit = ({
  offset, limit
}) => {
  let ret = {}
  if (offset !== undefined) {
    offset = parseInt(offset)
    if (!isNaN(offset) && offset >= 0) {
      ret.offset = offset
    }
  }
  if (limit !== undefined) {
    limit = parseInt(limit)
    if (!isNaN(limit) && limit > 0) {
      ret.limit = limit
    }
  }
  return ret
}

export const parse_sort_direction = ({
  query: {
    sort,
    sortDirection
  }, namemap
}) => {
  let ret = []
  if(Object.keys(namemap).includes(sort) && sort.toLowerCase() !== 'password') {
    sort = namemap[sort]
    if ([
      'ASC',
      'DESC'
    ].includes(sortDirection.toUpperCase())) {
      ret.push([
        sort,
        sortDirection
      ])
    } else {
      ret.push([
        sort,
        'ASC'
      ])
    }
  }
  return ret
}
