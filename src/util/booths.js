import models from '../models'
import {
  BadRequestError, NotFoundError, parse_offset_limit, parse_sort_direction
} from '.'

export const addBooth = async ({
  subjectId, pointReward, roomnumber
}) => {
  let subject = await models.Subjects.findOne({ where: { PK_SubjectID: subjectId } })

  if (!subject) {
    throw new BadRequestError('ungueltige subjectId')
  }

  await models.Booths.create({
    FK_Subject: subject.PK_SubjectID,
    PointReward: pointReward,
    Roomnumber: roomnumber
  })
}

export const getBooths = async query => {
  let offset_limit = parse_offset_limit(query)
  let sort = parse_sort_direction({
    query,
    namemap: {
      'id': 'PK_BoothID',
      'pointReward': 'PointReward',
      'roomnumber': 'Roomnumber',
      'subject.id': 'FK_Subject'
    }
  })
  let booths = await models.Booths.findAll({
    ...offset_limit,
    order: sort,
    include: [ {
      model: models.Subjects,
      attributes: [
        'PK_SubjectID',
        'Name'
      ]
    } ]
  })
  let cnt = await models.Booths.count()


  let offset = offset_limit.offset || 0
  let contentrange = `booths ${offset}-${offset + booths.length - 1}/${cnt}`

  return {
    '_data': booths.map(e => ({
      id: e.PK_BoothID,
      pointReward: e.PointReward,
      roomnumber: e.Roomnumber,
      subject: {
        id: e.Subject.PK_SubjectID,
        name: e.Subject.Name
      }
    })),
    '_headers': {
      'Content-Range': contentrange,
      'Access-Control-Expose-Headers': 'Content-Range'
    }
  }
}

const findBooth = async (id, include = []) => {
  let booth

  if (!isNaN(parseInt(id))) {
    booth = await models.Booths.findOne({
      where: { PK_BoothID: id },
      include
    })
  }

  return booth
}

export const getBooth = async id => {
  let booth = await findBooth(id, [ models.Subjects ])

  if (!booth) {
    throw new NotFoundError(`Booth ${id} not found`)
  }

  return {
    id: booth.PK_BoothID,
    pointReward: booth.PointReward,
    roomnumber: booth.Roomnumber,
    subject: {
      id: booth.Subject.PK_SubjectID,
      name: booth.Subject.Name,
      infotext: booth.Subject.Infotext
    }
  }
}

export const updateBooth = async (id, data) => {
  let booth = await findBooth(id)

  if (!booth) {
    throw new NotFoundError(`Booth ${id} not found`)
  }

  let newData = {}
  if (data.hasOwnProperty('pointReward')) {
    newData.PointReward = data.pointReward
  }
  if (data.hasOwnProperty('subjectId')) {
    let subject = await models.Subjects.findOne({ where: { PK_SubjectID: data.subjectId } })
    if (!subject) {
      throw new NotFoundError(`Subject ${id} not found`)
    }
    newData.FK_Subject = data.subjectId
  }
  if (data.hasOwnProperty('roomnumber')) {
    newData.Roomnumber = data.roomnumber
  }

  if (Object.keys(newData).length === 0) {
    throw new BadRequestError('No data to update')
  }

  await booth.update(newData)
}

export const deleteBooth = async id => {
  let booth = await findBooth(id)

  if (!booth) {
    throw new NotFoundError(`Booth ${id} not found`)
  }

  await models.UserScans.destroy({ where: { PK_FK_Booth: booth.PK_BoothID } })
  await models.sequelize.transaction(t => {
    let next = del => vbs => {
      if (vbs.length > 0) {
        if (del === false) {
          let el = vbs[vbs.length - 1]
          return el.Visitor.update({ Score: el.Visitor.Score - booth.PointReward }, { transaction: t }).then(() => next(true)(vbs))
        } else {
          let el = vbs.pop()
          return el.destroy({ transaction: t }).then(() => next(false)(vbs))
        }
      }
      return booth.destroy({ transaction: t })
    }
    return models.VisitedBooths.findAll({
      where: { FK_Booth: booth.PK_BoothID },
      include: [ models.Visitors ]
    }, { transaction: t }).then(next(false))
  })
}

export const getRooms = async () => {
  let booths = await models.Booths.findAll({ include: [ models.Subjects ] })

  let rooms = {}

  for (let i = 0; i < booths.length; i++) {
    let booth = booths[i]
    let roomnumber = booth.Roomnumber
    if (!rooms.hasOwnProperty(roomnumber)) {
      rooms[roomnumber] = []
    }
    rooms[roomnumber].push({
      id: booth.PK_BoothID,
      pointReward: booth.PointReward,
      subject: {
        id: booth.Subject.PK_SubjectID,
        name: booth.Subject.Name,
        infotext: booth.Subject.Infotext
      }
    })
  }

  return rooms
}
