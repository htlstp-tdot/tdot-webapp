import models from '../models'
import {
  BadRequestError, NotFoundError, parse_offset_limit, parse_sort_direction
} from '.'

export const addSubject = async ({
  name, infotext
}) => {
  await models.Subjects.create({
    Name: name,
    Infotext: infotext
  })
}

export const getSubjects = async query => {
  let offset_limit = parse_offset_limit(query)
  let sort = parse_sort_direction({
    query,
    namemap: {
      'id': 'PK_SubjectID',
      'name': 'Name',
      'infotext': 'Infotext'
    }
  })
  let subjects = await models.Subjects.findAll({
    ...offset_limit,
    order: sort
  })
  let cnt = await models.Subjects.count()


  let offset = offset_limit.offset || 0
  let contentrange = `subjects ${offset}-${offset + subjects.length - 1}/${cnt}`

  return {
    '_data': subjects.map(e => ({
      id: e.PK_SubjectID,
      name: e.Name,
      infotext: e.Infotext
    })),
    '_headers': {
      'Content-Range': contentrange,
      'Access-Control-Expose-Headers': 'Content-Range'
    }
  }
}

const findSubject = async id => {
  let subject

  if (!isNaN(parseInt(id))) {
    subject = await models.Subjects.findOne({ where: { PK_SubjectID: id } })
  }

  return subject
}

export const getSubject = async id => {
  let subject = await findSubject(id)

  if (!subject) {
    throw new NotFoundError(`Subject ${id} not found`)
  }

  return {
    id: subject.PK_SubjectID,
    name: subject.Name,
    infotext: subject.Infotext
  }
}

export const updateSubject = async (id, data) => {
  let subject = await findSubject(id)

  if (!subject) {
    throw new NotFoundError(`Subject ${id} not found`)
  }

  let newData = {}
  if (data.hasOwnProperty('name')) {
    newData.Name = data.name
  }
  if (data.hasOwnProperty('infotext')) {
    newData.Infotext = data.infotext
  }

  if (Object.keys(newData).length === 0) {
    throw new BadRequestError('No data to update')
  }

  await subject.update(newData)
}

export const deleteSubject = async id => {
  let subject = await findSubject(id)

  if (!subject) {
    throw new NotFoundError(`Subject ${id} not found`)
  }

  let booth = await models.Booths.findOne({ where: { FK_Subject: subject.PK_SubjectID } })

  if (booth) {
    throw new BadRequestError('Cannot delete subject with booths')
  }

  await subject.destroy()
}
