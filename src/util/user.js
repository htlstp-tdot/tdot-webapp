import models from '../models'
import bcrypt from 'bcrypt'
import {
  BadRequestError, NotFoundError, parse_offset_limit, parse_sort_direction
} from '.'

export const createUser = async ({
  username, password, roleId
}) => {
  let user = await models.Users.findOne({ where: { Username: username } })
  if (user) throw new BadRequestError('Username exists already')
  let role = await models.Roles.findOne({ where: { PK_RoleID: roleId } })
  if (!role) throw new BadRequestError('RoleID must be a valid Role ID')
  const hash = await bcrypt.hash(password, 10)
  await models.Users.create({
    Username: username,
    Password: hash,
    FK_Role: role.PK_RoleID
  })
}

export const getUsers = async query => {
  let offset_limit = parse_offset_limit(query)
  let sort = parse_sort_direction({
    query,
    namemap: {
      'id': 'PK_UserID',
      'name': 'Username',
      'role.id': 'FK_Role'
    }
  })
  let users = await models.Users.findAll({
    include: [
      { model: models.Roles },
      {
        model: models.UserScans,
        include: [ {
          model: models.Booths,
          include: [ { model: models.Subjects } ]
        } ]
      }
    ],
    ...offset_limit,
    order: sort
  })
  let cnt = await models.Users.count()

  let offset = offset_limit.offset || 0
  let contentrange = `users ${offset}-${offset + users.length - 1}/${cnt}`

  return {
    '_data': users.map(e => ({
      id: e.PK_UserID,
      username: e.Username,
      role: {
        id: e.Role.PK_RoleID,
        name: e.Role.Rolename,
        power: e.Role.Power
      },
      scanPermissions: e.UserScans.map(el => ({
        id: el.Booth.PK_BoothID,
        pointReward: el.Booth.PointReward,
        roomnumber: el.Booth.Roomnumber,
        subject: {
          id: el.Booth.Subject.PK_SubjectID,
          name: el.Booth.Subject.Name
        }
      }))
    })),
    '_headers': {
      'Content-Range': contentrange,
      'Access-Control-Expose-Headers': 'Content-Range'
    }
  }
}

const findUser = async id => {
  let user

  if (!isNaN(parseInt(id))) {
    user = await models.Users.findOne({
      where: { PK_UserID: id },
      include: [
        { model: models.Roles },
        {
          model: models.UserScans,
          include: [ {
            model: models.Booths,
            include: [ { model: models.Subjects } ]
          } ]
        }
      ]
    })
  }

  return user
}

export const getUser = async ({ id }) => {
  let user = await findUser(id)

  if (!user) {
    throw new NotFoundError(`User ${id} not found`)
  }

  return {
    id: user.PK_UserID,
    username: user.Username,
    role: {
      id: user.Role.PK_RoleID,
      name: user.Role.Rolename,
      power: user.Role.Power
    },
    scanPermissions: user.UserScans.map(el => ({
      id: el.Booth.PK_BoothID,
      pointReward: el.Booth.PointReward,
      roomnumber: el.Booth.Roomnumber,
      subject: {
        id: el.Booth.Subject.PK_SubjectID,
        name: el.Booth.Subject.Name
      }
    }))
  }
}

export const updateUser = async (id, data) => {
  let user = await findUser(id)

  if (!user) {
    throw new NotFoundError(`User ${id} not found`)
  }

  let newData = {}
  if (data.hasOwnProperty('password')) {
    newData.Password = await bcrypt.hash(data.password, 10)
  }
  if (data.hasOwnProperty('roleId')) {
    let role = await models.Roles.findOne({ where: { PK_RoleID: data.roleId } })
    if (!role) throw new BadRequestError('RoleID must be a valid Role ID')
    newData.FK_Role = data.roleId
  }

  if (Object.keys(newData).length === 0) {
    throw new BadRequestError('No data to update')
  }

  await models.Users.update(newData, { where: { PK_UserID: id } })
}

export const removeUser = async ({ id }) => {
  let user = await findUser(id)

  if (!user) {
    throw new NotFoundError(`User ${id} not found`)
  }

  await models.UserScans.destroy({ where: { PK_FK_User: id } })
  await models.Sessions.destroy({ where: { FK_User: id } })
  await models.Users.destroy({ where: { PK_UserID: id } })
}

export const addScanPermission = async ({
  id, boothId
}) => {
  let user = await findUser(id)

  if (!user) {
    throw new NotFoundError(`User ${id} not found`)
  }

  let booth = await models.Booths.findOne({ where: { PK_BoothID: boothId } })

  if (!booth) {
    throw new NotFoundError(`Booth ${boothId} not found`)
  }

  await models.UserScans.create({
    PK_FK_User: id,
    PK_FK_Booth: boothId
  })
}

export const removeScanPermission = async ({
  id, boothId
}) => {
  let userscan = await models.UserScans.findOne({ where: {
    PK_FK_User: id,
    PK_FK_Booth: boothId
  } })

  if (!userscan) {
    throw new NotFoundError('Scanpermission not found')
  }

  await userscan.destroy()
}
