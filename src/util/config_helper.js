import config from 'config'

export default key => {
  try {
    let val = process.env[key]
    if (!val) {
      val = config.get(key.replace(/_/g, '.').toLowerCase())
    }
    return val
  } catch (e) {
    throw new Error(`Ènvironment Variable ${key} not set and not in config file`)
  }
}
