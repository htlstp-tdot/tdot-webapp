import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { getConfig } from './util'
import cors from 'cors'
import helmet from 'helmet'
import compression from 'compression'

import { NotFoundError } from './util/errors'
import errorhandler from './middleware/errorhandler'
import router from './router'

morgan.token('pid', () => process.pid)

const launch = async ({
  log, port
} = {}) => {
  const app = express()

  if (log) {
    const loglevel = getConfig('LOGLEVEL')
    let format = ''
    if ([
      'combined',
      'common',
      'short',
      'tiny'
    ].includes(loglevel)) {
      format = morgan[loglevel]
    } else {
      format = loglevel
    }
    app.use(morgan(`PID :pid -> ${format}`))
  }

  app
    .use(compression())
    .use(cors())
    .use(helmet())
    .use(bodyParser.json())
    .use('/api', router)
    .all('*', (req, res, next) => {
      next(new NotFoundError())
    })
    .use(errorhandler(log))

  return new Promise(resolve => {
    const server = app.listen(port || getConfig('PORT'), '0.0.0.0', () => {
      console.log(`Server running on ${server.address().port} in process ${process.pid}`)
      resolve(server)
    })
  })
}

export default launch
