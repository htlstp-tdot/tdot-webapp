import { UnauthorizedError } from '../util/errors'
import models from '../models'

const checkAuth = minPower => async (req, res, next) => {
  const key = req.get('x-api-key')
  if (key) {
    const session = await models.Sessions.findOne({
      where: { Key: key },
      attributes: [],
      include: [ {
        model: models.Users,
        include: [ models.Roles ]
      } ]
    })
    if (session) {
      if (session.User.Role.Power >= minPower) {
        req.user = session.User
        req.sessionKey = key
        next()
      } else {
        next(new UnauthorizedError('Not enough permissions'))
      }
    } else {
      next(new UnauthorizedError('Invalid session'))
    }
  } else {
    next(new UnauthorizedError('Missing header'))
  }
}

export default checkAuth
