import {
  BadRequestError,
  addBooth,
  updateBooth as changeBooth,
  deleteBooth as removeBooth,
  getBooth as returnBooth,
  getBooths as returnBooths,
  getRooms as returnRooms
} from '../util'

export const createBooth = async req => {
  if (!req.body.hasOwnProperty('subjectId') || !req.body.hasOwnProperty('pointReward') || !req.body.hasOwnProperty('roomnumber'))
    throw new BadRequestError('subjectId, pointReward and roomnumber are required')
  return addBooth(req.body)
}

export const getBooths = async req => {
  return returnBooths(req.query)
}

export const getBooth = async req => {
  return returnBooth(req.params.id)
}

export const updateBooth = async req => {
  return changeBooth(req.params.id, req.body)
}

export const deleteBooth = async req => {
  return removeBooth(req.params.id)
}

export const getRooms = async () => {
  return returnRooms()
}
