import { BadRequestError } from '../util/errors'
import {
  logIn, logOut
} from '../util/auth'

export const login = async req => {
  if (!req.body.hasOwnProperty('username') || !req.body.hasOwnProperty('password')) throw new BadRequestError('username and password are required')
  return logIn(req.body)
}

export const logout = async req => {
  logOut({ key: req.sessionKey })
}

