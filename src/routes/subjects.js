import {
  BadRequestError,
  addSubject,
  updateSubject as changeSubject,
  deleteSubject as removeSubject,
  getSubject as returnSubject,
  getSubjects as returnSubjects
} from '../util'

export const createSubject = async req => {
  if (!req.body.hasOwnProperty('name') || !req.body.hasOwnProperty('infotext'))
    throw new BadRequestError('name and infotext are required')
  return addSubject(req.body)
}

export const getSubjects = async req => {
  return returnSubjects(req.query)
}

export const getSubject = async req => {
  return returnSubject(req.params.id)
}

export const updateSubject = async req => {
  return changeSubject(req.params.id, req.body)
}

export const deleteSubject = async req => {
  return removeSubject(req.params.id)
}
