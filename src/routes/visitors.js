import {
  BadRequestError,
  createVisit as addVisit,
  createVisitor as addVisitor,
  removeVisitor as deleteVisitor,
  getVisitor as returnVisitor,
  getVisitors as returnVisitors,
  calculateWinner as getWinner,
  UnauthorizedError
} from '../util'

import models from '../models'

export const getVisitors = async req => {
  return await returnVisitors(req.query)
}

export const createVisitor = async req => {
  if (!req.body.hasOwnProperty('email') || !req.body.hasOwnProperty('preregistration_isAllowed')) throw new BadRequestError('Email and preregistration consent are required')
  return addVisitor({
    email: req.body.email,
    preregistration_isAllowed: req.body.preregistration_isAllowed
  })
}

export const getVisitor = async req => {
  return returnVisitor(req.params.email)
}

export const createVisit = async req => {
  if (!req.body.hasOwnProperty('boothId')) throw new BadRequestError('BoothId in body is requires')
  if (req.user.Role.Power < 1000) {
    let perm = await models.UserScans.findOne({ where: {
      PK_FK_User: req.user.PK_UserID,
      PK_FK_Booth: req.body.boothId
    } })
    if (!perm) {
      throw new UnauthorizedError('No rights')
    }
  }
  return addVisit(req.params.email, req.body.boothId)
}

export const removeVisitor = async req => {
  return deleteVisitor(req.params.email)
}

export const calculateWinner = async req => {
  return getWinner(req.query)
}
