import status from './status'
import {
  login, logout
} from './auth'
import {
  createVisit, createVisitor, getVisitor, getVisitors, removeVisitor, calculateWinner
} from './visitors'
import { mockRoute } from './mock'
import {
  createRole, deleteRole, getRole, getRoles, updateRole
} from './role'
import {
  createUser, getCurrentUser, getUser, getUsers, removeUser, updateUser, addScanPermission, removeScanPermission
} from './user'
import {
  createBooth, deleteBooth, getBooth, getBooths, updateBooth, getRooms
} from './booths'
import {
  createSubject, deleteSubject, getSubject, getSubjects, updateSubject
} from './subjects'

const runRoute = route => async (req, res, next) => {
  try {
    const ret = await route(req, res)
    let data, headers
    if (typeof(ret) === 'object' && (ret.hasOwnProperty('_data') || ret.hasOwnProperty('_headers')) ) {
      data = ret['_data']
      headers = ret['_headers']
    } else {
      data = ret
    }

    if (headers) {
      res.set(headers)
    }

    if (data !== undefined) {
      res.status(200).json(data)
    } else {
      res.status(204).end()
    }
  } catch (e) {
    next(e)
  }
}

export default runRoute
export {
  status,
  login,
  logout,
  getCurrentUser,
  createVisitor,
  getVisitors,
  getVisitor,
  mockRoute,
  createRole,
  getRoles,
  getRole,
  updateRole,
  deleteRole,
  createUser,
  getUsers,
  getUser,
  updateUser,
  removeUser,
  createVisit,
  removeVisitor,
  createBooth,
  deleteBooth,
  getBooth,
  getBooths,
  updateBooth,
  createSubject,
  deleteSubject,
  getSubject,
  getSubjects,
  updateSubject,
  getRooms,
  calculateWinner,
  addScanPermission,
  removeScanPermission
}
