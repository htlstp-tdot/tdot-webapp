import {
  BadRequestError,
  createUser as addUser,
  updateUser as changeUser,
  removeUser as deleteUser,
  getUser as returnUser,
  getUsers as returnUsers,
  addScanPermission as createScanPermission,
  removeScanPermission as deleteScanPermission
} from '../util'

export const getCurrentUser = async req => {
  return returnUser({ id: req.user.PK_UserID })
}

export const createUser = async req => {
  if (!req.body.hasOwnProperty('username') || !req.body.hasOwnProperty('password') || !req.body.hasOwnProperty('roleId'))
    throw new BadRequestError('username, password and roleId are required')

  return addUser(req.body)
}

export const getUsers = async req => {
  return returnUsers(req.query)
}

export const getUser = async req => {
  return returnUser(req.params)
}

export const updateUser = async req => {
  return changeUser(req.params.id, req.body)
}

export const removeUser = async req => {
  let { id } = req.params
  if (id == req.user.PK_UserID) {
    throw new BadRequestError('Cannot delete own user')
  }

  return deleteUser(req.params)
}

export const addScanPermission = async req => {
  return createScanPermission(req.params)
}

export const removeScanPermission = async req => {
  return deleteScanPermission(req.params)
}
