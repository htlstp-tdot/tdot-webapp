import {
  BadRequestError,
  addRole,
  updateRole as changeRole,
  deleteRole as removeRole,
  getRole as returnRole,
  getRoles as returnRoles
} from '../util'

export const createRole = async req => {
  if (!req.body.hasOwnProperty('rolename') || !req.body.hasOwnProperty('power')) throw new BadRequestError('rolename and power are required')
  return addRole(req.body)
}

export const getRoles = async req => {
  return returnRoles(req.query)
}

export const getRole = async req => {
  return returnRole(req.params.id)
}

export const updateRole = async req => {
  let { id } = req.params
  if (id == req.user.PK_UserID) {
    throw new BadRequestError('Cannot change own role')
  }

  return changeRole(id, req.body)
}

export const deleteRole = async req => {
  let { id } = req.params

  return removeRole(id)
}
